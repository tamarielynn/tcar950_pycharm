import csv
import datetime


# Student ID: [011420359]


class ChainingHashTable:
    def __init__(self, initial_capacity=40):
        # Initialize the hash table with an empty list for each bucket
        self.table = []
        for _ in range(initial_capacity):
            self.table.append([])

    def __str__(self):
        # Convert the hash table to a string for easy printing
        result = ""
        for bucket in self.table:
            if bucket:
                for key, value in bucket:
                    result += f"{key}: {value}\n"
        return result

    def insert(self, key, item):
        # Insert a key-value pair into the hash table
        bucket = hash(key) % len(self.table)
        bucket_list = self.table[bucket]

        for i, kv in enumerate(bucket_list):
            if kv[0] == key:
                # Update the value if the key already exists
                bucket_list[i] = (key, item)
                return
        # Add a new key-value pair if the key doesn't exist
        key_value = (key, item)
        bucket_list.append(key_value)

    # Check load factor and resize if necessary
    def search(self, key):
        # Search for a key in the hash table and return its corresponding value
        bucket = hash(key) % len(self.table)
        bucket_list = self.table[bucket]

        for kv in bucket_list:
            if kv[0] == key:
                return kv[1]
        return None

    def remove(self, key):
        # Remove a key-value pair from the hash table
        bucket = hash(key) % len(self.table)
        bucket_list = self.table[bucket]

        for kv in bucket_list:
            if kv[0] == key:
                bucket_list.remove(kv)
                return

    def values(self):
        # Generate all values in the hash table
        for bucket_list in self.table:
            for key, value in bucket_list:
                yield value

    def update(self, key, updated_item):
        # Update the value associated with a key in the hash table
        bucket = hash(key) % len(self.table)
        bucket_list = self.table[bucket]

        for i, (existing_key, existing_item) in enumerate(bucket_list):
            if existing_key == key:
                bucket_list[i] = (key, updated_item)
                return

        # If key is not found, you can raise an exception or handle it accordingly
        raise KeyError(f"Key '{key}' not found in the hash table")

    # Get a list of all keys in the hash table
    def get_all_keys(self):
        keys = []
        for bucket_list in self.table:
            for key, _ in bucket_list:
                keys.append(key)
        return keys
    # Insertion: O(1) on average, assuming a good hash function and load factor. Search: O(1) on average, assuming a
    # good hash function and load factor. Deletion: O(1) on average, assuming a good hash function and load factor.
    # Get All Keys: O(N + M), where N is the number of buckets and M is the total number of key-value pairs. #This is
    # because you need to iterate over all buckets and all key-value pairs in the buckets.


class Package:
    def __init__(self, ID, address, city, state, zipcode, deadline_time, weight, status):
        # Initialize package attributes
        self.delivery_status = None  # Dictionary to store delivery status information
        self.ID = ID
        self.address = address
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.deadline_time = deadline_time
        self.weight = weight
        self.status = status
        self.delivery_time = None
        self.departure_time = None

    def __str__(self):
        # Convert the package information to a string for easy printing
        delivery_time_str = str(self.delivery_time) if self.delivery_time else "Not delivered"
        return "%s, %s,%s, %s,%s, %s,%s, %s, %s" % (
            self.ID, self.address, self.city, self.state, self.zipcode,
            self.deadline_time, self.weight, self.status, delivery_time_str)

    def update_status(self, convert_timedelta):
        try:
            # Ensure self.delivery_time and convert_timedelta are both not None
            if self.delivery_time and convert_timedelta:
                # Extract hours and minutes from convert_timedelta
                convert_hours, convert_minutes = divmod(convert_timedelta.seconds // 60, 60)

                # Extract hours and minutes from self.delivery_time
                delivery_hours, delivery_minutes = divmod(self.delivery_time.seconds // 60, 60)

                # Check if self.delivery_time is earlier than convert_timedelta
                if (delivery_hours, delivery_minutes) < (convert_hours, convert_minutes):
                    self.status = "Delivered"
                elif self.departure_time > convert_timedelta:
                    self.status = "En route"
                else:
                    self.status = "At Hub"
            else:
                raise ValueError("Error: Cannot update status. Delivery time or provided time is not available.")

            # Update the delivery_status dictionary
            self.delivery_status = {'status': self.status, 'delivery_time': self.delivery_time}
        except Exception as e:
            print(f"An error occurred: {e}")

            # Time Complexity:
            # - The time complexity of the __init__ method is O(1) because it performs constant time operations.
            # - The time complexity of the __str__ method is O(1) because it performs constant time operations.
            # - The time complexity of the update_status method is O(1) because it performs constant time operations.


class Truck:
    def __init__(self, truck_id, capacity, average_speed, load, package_ids, mileage, current_location, departure_time):
        # Initialize truck attributes
        self.truck_id = truck_id
        self.capacity = capacity
        self.average_speed = average_speed
        self.load = load
        self.package_ids = package_ids
        self.mileage = mileage
        self.current_location = current_location
        self.departure_time = departure_time

    def __str__(self):
        # Convert truck information to a string for easy printing
        return "%s, %s, %s, %s, %s, %s, %s, %s" % (self.truck_id, self.capacity, self.average_speed, self.load,
                                                   self.package_ids, self.mileage, self.current_location,
                                                   self.departure_time)

    def calculate_travel_time(self, distance):
        # Calculate travel time based on the provided distance and average speed
        return datetime.timedelta(hours=distance / self.average_speed)
    # Time Complexity:
    # - The time complexity of the __init__ method is O(1) because it performs constant time operations.
    # - The time complexity of the __str__ method is O(1) because it performs constant time operations.
    # - The time complexity of the calculate_travel_time method is O(1) because it performs constant time operations.


# Read the file of distance information
with open("Distance_info.csv") as csvfile:
    CSV_Distance = csv.reader(csvfile)
    CSV_Distance = list(CSV_Distance)

# Read the file of address information
with open("Address_file.csv") as csvfile1:
    CSV_Address = csv.reader(csvfile1)
    CSV_Address = list(CSV_Address)

# Read the file of package information
with open("Package_info.csv") as csvfile2:
    CSV_Package = csv.reader(csvfile2)
    CSV_Package = list(CSV_Package)


def load_package_data(filename, package_hash_table):
    # Open the package information file
    with open(filename) as package_info:
        # Read the CSV data
        package_data = csv.reader(package_info)
        next(package_data)  # Skip header row
        # Iterate over each package in the CSV data
        for package in package_data:
            pID = int(package[0])
            pAddress = package[1]
            pCity = package[2]
            pState = package[3]
            pZipcode = package[4]
            pDeadline_time = package[5]
            pWeight = package[6]
            pStatus = "At Hub"

            # # Set a default delivery time
            # pDelivery_time = "12:00 AM"

            # Package object
            p = Package(pID, pAddress, pCity, pState, pZipcode, pDeadline_time, pWeight, pStatus)

            # Insert data into hash table
            package_hash_table.insert(pID, p)


# Method for finding distance between two addresses
def distance_in_between(x_value, y_value):
    # Get distance from the Distance_info CSV
    distance = CSV_Distance[x_value][y_value]
    if distance == '':
        # If the distance is not available, try the reverse
        distance = CSV_Distance[y_value][x_value]
    return float(distance)


# Method to get address number from string literal of address
def extract_address(address):
    # Iterate over rows in the CSV_Address data
    for row in CSV_Address:
        if address in row[2]:
            # Return the address number
            return int(row[0])


# Time Complexity:
# - The time complexity of load_package_data is O(n), where n is the number of packages in the CSV file.
# - The time complexity of distance_in_between is O(1) because it performs constant time operations.
# - The time complexity of extract_address is O(k), where k is the number of rows in the CSV_Address data.


# Create truck object truck1
truck1 = Truck(1, 16, 18, None, [31, 6, 25, 26, 16, 15, 14, 40, 20, 21, 1, 37, 29, 13, 30, 19], 0.0,
               "4001 South 700 East",
               datetime.timedelta(hours=8))

# Create truck object truck2
truck2 = Truck(2, 16, 18, None, [3, 18, 36, 38, 39, 33, 2, 4, 7, 5, 8, 22, 24, 35, 32, 34, 28], 0.0,
               "4001 South 700 East", datetime.timedelta(hours=9, minutes=5))

# Create truck object truck3
truck3 = Truck(3, 16, 18, None, [9, 10, 11, 12, 17, 23, 27], 0.0, "4001 South 700 East",
               datetime.timedelta(hours=10, minutes=20))
truck1.departure_time = datetime.timedelta(hours=8, minutes=5)
truck2.departure_time = datetime.timedelta(hours=8, minutes=5)
truck3.departure_time = datetime.timedelta(hours=10, minutes=20)
trucks = [truck1, truck2, truck3]

# Create hash table
package_hash_table = ChainingHashTable()

# Load packages into hash table
load_package_data("Package_info.csv", package_hash_table)


def delivering_packages(truck, package_hash_table):
    # List to store packages not yet delivered by the truck
    not_delivered = []

    # Helper function to calculate delivery time based on travel time
    def calculate_delivery_time(travel_time):
        return truck.departure_time + travel_time

        # Helper function to parse time in "HH:MM AM/PM" format to datetime.timedelta

    def parse_time(time_str):
        time_format = "%I:%M %p"
        parsed_time = datetime.datetime.strptime(time_str, time_format).time()
        return datetime.timedelta(hours=parsed_time.hour, minutes=parsed_time.minute)

    for packageID in truck.package_ids:
        package = package_hash_table.search(packageID)
        not_delivered.append(package)

    # Clear the package IDs from the truck, as they will be reloaded
    truck.package_ids.clear()

    # Loop until all packages are delivered
    while len(not_delivered) > 0:
        # Find the next package with the closest distance to the truck's current location
        next_address = 2000
        next_package = None

        for package in not_delivered:
            distance = distance_in_between(extract_address(truck.current_location),
                                           extract_address(package.address))
            if distance <= next_address:
                next_address = distance
                next_package = package

        # Load the next package onto the truck
        truck.package_ids.append(next_package.ID)
        not_delivered.remove(next_package)
        truck.mileage += next_address
        truck.current_location = next_package.address
        # Calculate travel time for the truck to reach the next delivery location
        travel_time = truck.calculate_travel_time(next_address)

        # Update the truck's departure time and the package's delivery time
        truck.departure_time += travel_time
        next_package.delivery_time = calculate_delivery_time(travel_time)
        next_package.departure_time = truck.departure_time

        # Convert deadline_time to datetime.timedelta
        next_package_deadline_time = parse_time(next_package.deadline_time)

        # Update the package status directly
        if next_package_deadline_time is not None:
            if next_package.delivery_time < next_package_deadline_time:
                next_package.status = "Delivered"
            elif truck.departure_time > next_package_deadline_time:
                next_package.status = "En route"
            else:
                next_package.status = "At Hub"


# Put the trucks through the loading process
delivering_packages(truck1, package_hash_table)
delivering_packages(truck2, package_hash_table)
truck3.departure_time = min(truck1.departure_time, truck2.departure_time)
delivering_packages(truck3, package_hash_table)


def get_user_input_time():
    while True:
        try:
            user_input = input("Enter the time in HH:MM AM/PM format: ")
            time_format = "%I:%M %p"
            parsed_time = datetime.datetime.strptime(user_input, time_format).time()
            return parsed_time
        except ValueError:
            print("Invalid time format. Please enter the time in HH:MM AM/PM format.")


def get_package_data():
    print("Package Information from HashTable:")

    for bucket in package_hash_table.table:
        for kv in bucket:
            package = kv[1]
            print(f"Package ID: {package.ID}")
            print(f"Address: {package.address}")
            print(f"City: {package.city}")
            print(f"State: {package.state}")
            print(f"Zipcode: {package.zipcode}")
            print(f"Deadline Time: {package.deadline_time}")
            print(f"Weight: {package.weight}")
            print(f"Status: {package.status}")
            print(f"Delivery Time: {package.delivery_time}")
            print(f"Departure Time: {package.departure_time}")
            print("\n")


def display_truck_information():
    print(truck1.mileage + truck2.mileage + truck3.mileage)


# UI start include delivery time and weight
def get_user_input():
    try:
        package_id = int(input("Enter the package ID: "))
        return package_id
    except ValueError:
        print("Invalid input. Please enter a valid integer.")
        return None


def view_delivery_status(package_hash_table, trucks):
    print("View Delivery Status")
    print("1- View status between 8:35 a.m. and 9:25 a.m.")
    print("2- View status between 9:35 a.m. and 10:25 a.m.")
    print("3- View status between 12:03 p.m. and 1:12 p.m.")

    choice = input("Enter your choice:")

    if choice == '1':
        start_time = datetime.timedelta(hours=8, minutes=35)
        end_time = datetime.timedelta(hours=9, minutes=25)
    elif choice == '2':
        start_time = datetime.timedelta(hours=9, minutes=35)
        end_time = datetime.timedelta(hours=10, minutes=25)

    elif choice == '3':
        start_time = datetime.timedelta(hours=12, minutes=3)
        end_time = datetime.timedelta(hours=13, minutes=12)
    elif choice == '4':
        return
    else:
        print("Invalid choice. Please enter a valid option.")
        return

    print("\nDelivery Status:")
    for truck in trucks:
        print(f"\nTruck {truck.truck_id}:")
        for package_id in truck.package_ids:
            package = package_hash_table.search(package_id)
            print(f"Package {package_id} - {package.status} at {package.delivery_time}")


# Run the main home_page function
def home_page():
    print("Total mileage for all three trucks:")
    print(display_truck_information())

    while True:
        print("Select from the following: "
              "\n1 - Delivery Status"
              "\n2 - Print all packages"
              "\n3 - End program")

        choice = input("Enter your choice: ")

        if choice == '1':
            view_delivery_status(package_hash_table, trucks)
            break

        elif choice == '2':
            get_package_data()
            break

        elif choice == '3':
            print("Ending program.")
            break

        else:
            print("Invalid choice. Please enter a valid option.")


home_page()
